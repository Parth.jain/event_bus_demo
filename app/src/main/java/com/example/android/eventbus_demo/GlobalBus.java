package com.example.android.eventbus_demo;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by parth on 27/3/18.
 */

public class GlobalBus {
    private static EventBus sBus;
    public static EventBus getBus() {
        if (sBus == null)
            sBus = EventBus.getDefault();
        return sBus;
    }
}
