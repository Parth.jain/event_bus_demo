package com.example.android.eventbus_demo;

/**
 * Created by parth on 27/3/18.
 */

public class Events {

    public static class ActivityFragmentMessage {
        private String message;
        public ActivityFragmentMessage(String message){
            this.message=message;
        }
        public String getMessage(){
            return message;
        }
    }
    // Event used to send message from fragment to activity.
    public static class FragmentActivityMessage {
        private String message;
        public FragmentActivityMessage(String message) {
            this.message = message;
        }
        public String getMessage() {
            return message;
        }
    }

   /* public static class ActivityActivityMessage{
        private String message;
        public ActivityActivityMessage(String message){
            this.message=message;
        }
        public String getMessage(){
            return message;
        }
    }*/
}
